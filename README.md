# gmsh_python

Ubuntu image with recent versions of 
* [Gmsh](https://gmsh.info/) 
* [Eigen](https://eigen.tuxfamily.org/)
* [Python3](https://www.python.org/)
* [SWIG](https://www.swig.org/)
